const colors = require("tailwindcss/colors");

module.exports = {
	purge: ["./*.html", "./src/**/*.{js,jsx,ts,tsx,vue}"],
	theme: {
		extend: {
			colors: {
				"light-blue": colors.lightBlue,
			},
		},
	},
	plugins: [require("@tailwindcss/forms")],
};
