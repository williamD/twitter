import React, { useState, useEffect, createContext } from "react";
import { Switch, Route, useLocation, useHistory } from "react-router-dom";
import http from "@/api.js";
import Header from "./components/Header";
import SubscriptionList from "./components/SubscriptionList";
import TweetList from "./components/TweetList";
import Footer from "@/components/Footer";
import Pagination from "@/components/Pagination";
import Select from "@/components/Select";

function useQuery() {
	return new URLSearchParams(useLocation().search);
}
const subscriptions = [
	{ name: "QEP 1", time: 0.5, energy: 0.012, cost: 0.08 },
	{ name: "QEP 2", time: 3.0, energy: 0.3, cost: 0.05 },
	{ name: "QEP 3", time: 0.6, energy: 0.013, cost: 0.055 },
];
const limits = [
	{ id: 15, name: "Afficher 15" },
	{ id: 30, name: "Afficher 30" },
	{ id: 50, name: "Afficher 50" },
	{ id: 100, name: "Afficher 100" },
];
export const KeyWordsContext = createContext();

const Home = () => {
	const query = useQuery();
	const location = useLocation();
	const history = useHistory();
	const [tweetsLoading, setTweetsLoading] = useState([]);
	const [keyWords, setKeyWords] = useState([]);
	const [totalCost, setTotalCost] = useState(JSON.parse(localStorage.getItem("cost")) ? JSON.parse(localStorage.getItem("cost")).total : 0);
	const [subscriptionSelected, setSubscriptionSelected] = useState(
		JSON.parse(localStorage.getItem("subscription")) ? JSON.parse(localStorage.getItem("subscription")) : subscriptions[0]
	);

	const [totalResults, setTotalResults] = useState(0);
	const [currentPage, setCurrentPage] = useState(1);
	const [totalPages, setTotalPages] = useState(1);
	const [firstPage, setFirstPage] = useState(1);
	const [lastPage, setLastPage] = useState(1);
	const [previousPage, setPreviousPage] = useState(1);
	const [nextPage, setNextPage] = useState(1);
	const [limit, setLimit] = useState(15);

	// récuperation des tweets /api/search
	const [tweets, setTweets] = useState([]);

	const time = localStorage.getItem("subscription") ? parseFloat(JSON.parse(localStorage.getItem("subscription")).time) : subscriptions[0].time;
	const getTweets = async (page) => {
		setTweetsLoading(true);
		const { data } = await http.post(`/search`, {
			query: keyWords,
			page: page ? +page : 1,
			sub: {
				time: time,
			},
			limit,
			sort: { "data.created_at": "desc" },
		});
		setTweetsLoading(false);
		calculTotalCost();
		setTweets(data.docs);
		setCurrentPage(data.page);
		setTotalPages(data.totalPages);
		setLastPage(data.totalPages);
		setPreviousPage(data.prevPage);
		setNextPage(data.nextPage);
		setTotalResults(data.totalDocs);
	};
	useEffect(() => {
		getTweets(1);
		history.push("/");
	}, [keyWords, limit]);

	/* Calcul du coût total à partir du localStorage */
	const calculTotalCost = () => {
		let total = totalCost;
		total += parseFloat(localStorage.getItem("subscription") ? JSON.parse(localStorage.getItem("subscription")).cost : subscriptions[0].cost);
		localStorage.setItem("cost", JSON.stringify({ total: total }));
		setTotalCost(total);
	};

	/* Suppression d'un mot clé */
	const removeKeyWord = (name) => {
		setKeyWords(keyWords.filter((res) => res !== name));
	};

	/* Ajout d'un mot clé */
	const addKeyWord = (name) => {
		let everAdd = false;
		// si le mot clé existe déjà, il n'est pas ajouté
		keyWords.forEach((el) => {
			if (el === name) {
				everAdd = true;
			}
		});

		if (!everAdd) {
			setKeyWords([...keyWords, name]);
		}
	};

	/* Ajout des informations de l'abonnement dans le localStorage */
	const addSubscription = (subscription) => {
		localStorage.setItem("subscription", JSON.stringify(subscription));
	};

	useEffect(() => {
		const page = query.get("page");
		setCurrentPage(page);
		getTweets(page);
	}, [location]);

	return (
		<Switch>
			<Route exact path="/">
				<KeyWordsContext.Provider
					value={{
						keyWords,
						setKeyWords,
						removeKeyWord,
						addKeyWord,
						subscriptionSelected,
						setSubscriptionSelected,
						addSubscription,
						totalCost,
					}}
				>
					<Header />
					<div className="relative px-4 sm:px-6 lg:px-8 my-8">
						<div className="mx-auto max-w-7xl grid grid-cols-1 gap-4 items-start lg:grid-cols-3 lg:gap-8">
							<SubscriptionList data={subscriptions} />

							<div className="grid grid-cols-1 gap-4 lg:col-span-2">
								<div className="flex justify-between items-end">
									<div className="flex space-x-1 text-sm text-gray-700">
										<span className="font-medium">{totalResults.toLocaleString(undefined)}</span>
										<span>résultats</span>
									</div>
									<Select
										data={limits}
										size="w-64"
										setSelected={(event) => setLimit(event.id)}
										initialSelected={limits.find((limitItem) => limitItem.id === limit)}
									/>
								</div>
								<TweetList tweets={tweets} loading={tweetsLoading} limit={limit} />
							</div>
						</div>
						<div className="mx-auto max-w-7xl grid grid-cols-1 gap-4 items-start lg:grid-cols-3 lg:gap-8">
							<div className="grid grid-cols-1 gap-4"></div>
							<div className="grid grid-cols-1 gap-4 lg:col-span-2">
								<Pagination
									currentPage={currentPage}
									totalPages={totalPages}
									firstPage={firstPage === currentPage ? null : firstPage}
									lastPage={lastPage === currentPage ? null : lastPage}
									previousPage={previousPage < 1 ? null : previousPage}
									nextPage={nextPage > totalPages ? null : nextPage}
								/>
							</div>
						</div>
					</div>
					<Footer />
				</KeyWordsContext.Provider>
			</Route>
			<Route path="*">
				<div>Page not found</div>
			</Route>
		</Switch>
	);
};

export default Home;
