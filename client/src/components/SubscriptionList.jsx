import React, { useContext } from "react";
import Subscription from "./Subscription";
import { KeyWordsContext } from "@/Home";

const SubscriptionList = ({ data }) => {
	const { totalCost, keyWords } = useContext(KeyWordsContext);

	return (
		<div className={`${keyWords && keyWords.length > 0 ? "sm:top-36" : "sm:top-24"} static lg:sticky grid grid-cols-1 gap-4`}>
			<div className="bg-white overflow-hidden shadow rounded-lg">
				<div className="px-4 py-5 sm:p-6">
					<fieldset className="w-full">
						<legend className="sr-only">Privacy setting</legend>
						<div className="bg-white rounded-md -space-y-px">
							{data.map((subscription, index) => (
								<Subscription key={index} data={subscription} top={index === 0} bottom={index === data.length - 1} />
							))}
						</div>
					</fieldset>
					<div className="flex justify-between mt-4">
						<span>Coût total</span>
						<span className="font-medium">${totalCost?.toFixed(3)}</span>
					</div>
				</div>
			</div>
		</div>
	);
};

export default SubscriptionList;
