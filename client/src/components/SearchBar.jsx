import React, { useContext, useState } from "react";
import { KeyWordsContext } from "../Home";

const SearchBar = () => {
	const [searchValue, setSearchValue] = useState("");
	const { addKeyWord } = useContext(KeyWordsContext);

	const handleChangeSearch = (event) => {
		const { value } = event.target;
		setSearchValue(value);
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		if (searchValue !== "") {
			addKeyWord(searchValue);
			setSearchValue("");
		}
	};

	return (
		<form onSubmit={handleSubmit} className="flex w-full space-x-2 items-center justify-center">
			<div className="flex justify-center items-center w-full max-w-lg">
				<div className="w-full">
					<label htmlFor="search" className="sr-only">
						Type keyword
					</label>
					<div className="relative text-white focus-within:text-gray-400">
						<div className="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center">
							<svg
								className="h-5 w-5 flex-shrink-0"
								xmlns="http://www.w3.org/2000/svg"
								viewBox="0 0 20 20"
								fill="currentColor"
								aria-hidden="true"
							>
								<path
									fillRule="evenodd"
									d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
									clipRule="evenodd"
								/>
							</svg>
						</div>
						<input
							id="search"
							name="search"
							className="block w-full bg-light-blue-400 border-transparent py-2 pl-10 pr-3 text-base leading-5 placeholder-white focus:outline-none focus:bg-white focus:ring-0 focus:border-white focus:text-gray-900 focus:placeholder-gray-400 sm:text-sm rounded-md"
							placeholder="Rechercher des tweets"
							value={searchValue}
							onChange={handleChangeSearch}
							type="search"
						/>
					</div>
				</div>
			</div>
			<button
				type="submit"
				className="hidden md:block items-center px-3 py-2 border border-transparent text-sm font-medium rounded-md text-light-blue-700 bg-white hover:bg-light-blue-50 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-offset-light-blue-500 focus-visible:ring-white"
			>
				Ajouter le mot clé
			</button>
			<button
				type="submit"
				className="md:hidden inline-flex items-center p-2 border border-transparent rounded-full shadow-sm text-light-blue-500 bg-white hover:bg-light-blue-50 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-offset-light-blue-500 focus-visible:ring-white"
			>
				<svg className="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
					<path fillRule="evenodd" d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z" clipRule="evenodd" />
				</svg>
			</button>
		</form>
	);
};

export default SearchBar;
