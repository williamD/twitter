import React from "react";

const Tweet = ({ imgURL, name, username, text, verified, medias, id }) => {
	return (
		<div
			onClick={() => {
				window.open(`https://twitter.com/${username}/status/${id}`, "_blank");
			}}
			className="cursor-pointer p-3 relative rounded-lg border border-gray-200 bg-white shadow-sm flex items-center flex-col md:flex-row space-x-3 hover:border-gray-300 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500"
		>
			<div className="sm:self-center md:self-start flex-shrink-0">
				{imgURL ? (
					<img className="inline-block h-12 rounded-md" src={imgURL} alt="" />
				) : (
					<span class="inline-block h-10 w-10 rounded-full overflow-hidden bg-gray-100">
						<svg class="h-full w-full text-gray-300" fill="currentColor" viewBox="0 0 24 24">
							<path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
						</svg>
					</span>
				)}
			</div>
			<div className="flex-1 min-w-0">
				<div className="flex flex-col focus:outline-none">
					<div>
						<span className="absolute inset-0" aria-hidden="true"></span>
						<div className="flex flex-wrap sm:justify-center md:justify-start">
							<p className="text-sm font-medium text-gray-900 mr-2">{name}</p>
							<p className="text-sm font-medium text-gray-900">@{username}</p>
						</div>
						<p className="text-sm text-gray-500">{text}</p>
					</div>
					<div className="w-auto flex flex-wrap">
						{medias
							? medias.map((el, index) => {
									if (el.length === 2) {
										const img = el[1];
										if (el[0] === "photo") {
											return <img key={index} className="object-cover w-20 shadow-lg rounded-lg mr-3 mt-3" src={img} alt="" />;
										} else if (el[0] === "video") {
											return <img key={index} className="object-cover w-20 shadow-lg rounded-lg mr-3 mt-3" src={img} alt="" />;
										}
									}
							  })
							: ""}
					</div>
				</div>
			</div>
		</div>
	);
};

export default Tweet;
