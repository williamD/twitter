import React, { useContext } from "react";
import SearchBar from "./SearchBar";
import KeywordList from "@/components/KeywordList.jsx";

const Header = () => {
	return (
		<>
			<header className="bg-light-blue-500 shadow-sm sticky w-full z-20 inset-x-0 top-0 lg:overflow-y-visible">
				<div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
					<div className="relative flex justify-between items-center h-16">
						<div className=" flex justify-center items-center min-w-0 flex-1 md:px-8 lg:px-0 xl:col-span-6">
							<SearchBar />
						</div>
					</div>
				</div>
				<KeywordList />
			</header>
		</>
	);
};

export default Header;
