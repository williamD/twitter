import React, { Suspense } from "react";
import Tweet from "./Tweet";
import TweetSkeleton from "@/components/TweetSkeleton";

const TweetList = ({ tweets, loading }) => {
	const rangeArray = [...Array(100).keys()];
	const isObjEmpty = (obj) => {
		for (let prop in obj) {
			if (obj.hasOwnProperty(prop)) return false;
		}

		return true;
	};
	return (
		<div className="relative" style={{ minHeight: "950px" }}>
			{loading && (
				<div className="absolute w-full h-full bg-white overflow-hidden shadow rounded-lg z-10">
					<div className="px-4 py-5 sm:p-6 space-y-4">
						{rangeArray.map((value) => (
							<TweetSkeleton key={value} />
						))}
					</div>
				</div>
			)}
			<div className="bg-white overflow-hidden shadow rounded-lg">
				<div className="px-4 py-5 sm:p-6 space-y-4">
					{tweets?.map((tweet) => {
						let name,
							username,
							profile_image_url,
							verified = "";
						let medias = [];
						const id = tweet.data.id;
						if (tweet.includes.users.length > 0) {
							profile_image_url = tweet.includes.users[0].profile_image_url;
							name = tweet.includes.users[0].name;
							username = tweet.includes.users[0].username;
							verified = tweet.includes.users[0].verified;
						}

						if (!isObjEmpty(tweet.includes.media) && tweet.includes.media.length > 0) {
							medias = tweet.includes.media.map((el) => {
								let url = el.url;
								if (url === undefined) {
									url = el.preview_image_url;
								}
								return [el.type, url];
							});
						}
						return (
							<Tweet
								key={`tweet-${id}`}
								imgURL={profile_image_url}
								name={name}
								username={username}
								text={tweet.data.text}
								verified={verified}
								medias={medias}
								id={id}
							/>
						);
					})}
				</div>
			</div>
		</div>
	);
};

export default TweetList;
