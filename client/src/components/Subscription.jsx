import React, { useContext } from "react";
import { KeyWordsContext } from "../Home";

const Subscription = ({ data, top, bottom }) => {
	const { subscriptionSelected, setSubscriptionSelected, addSubscription } = useContext(KeyWordsContext);

	const handleClickSubscription = (event) => {
		setSubscriptionSelected(data);
		addSubscription(data);
	};

	const getClassRounded = () => {
		let res = "";
		if (subscriptionSelected.name === data?.name) {
			res = "bg-light-blue-50 border-light-blue-200 z-10";
			if (top) {
				res = `${res} rounded-tl-md rounded-tr-md`;
			} else if (bottom) {
				res = `${res} rounded-bl-md rounded-br-md`;
			}
		} else {
			res = "bg-white-50 border-gray-200";
			if (top) {
				res = `${res} rounded-tl-md rounded-tr-md`;
			} else if (bottom) {
				res = `${res} rounded-bl-md rounded-br-md`;
			}
		}
		return `${res} relative border p-4 flex cursor-pointer w-full`;
	};

	return (
		<label className={getClassRounded()}>
			<input
				onChange={handleClickSubscription}
				type="radio"
				name="QEP"
				checked={subscriptionSelected.name === data?.name}
				value={data?.name}
				className="h-4 w-4 mt-0.5 cursor-pointer text-light-blue-600 border-gray-300 focus:ring-light-blue-500"
				aria-labelledby="privacy-setting-0-label"
				aria-describedby="privacy-setting-0-description"
			/>
			<div className="ml-3 w-full flex flex-col">
				<div className="flex justify-between">
					<span
						id="privacy-setting-0-label"
						className={
							subscriptionSelected === data?.name ? "text-light-blue-900 block text-sm font-medium" : "text-gray-900 block text-sm font-medium"
						}
					>
						{data?.name}
					</span>
					<span
						className={
							subscriptionSelected === data?.name ? "text-light-blue-900 block text-sm font-medium" : "text-gray-900 block text-sm font-medium"
						}
					>
						${data?.cost?.toFixed(3)}
					</span>
				</div>
				<div className="flex justify-between">
					<span
						id="privacy-setting-0-description"
						className={subscriptionSelected === data?.name ? "text-light-blue-700 block text-sm" : "text-gray-500 block text-sm"}
					>
						{data?.time} s · {data?.energy}mA
					</span>
					<span className={subscriptionSelected === data?.name ? "text-light-blue-700 block text-sm " : "text-gray-500 block text-sm"}>/req</span>
				</div>
			</div>
		</label>
	);
};

export default Subscription;
