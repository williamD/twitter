import React from "react";

const Footer = () => {
	return (
		<footer className="bg-white">
			<div className="max-w-7xl mx-auto py-12 px-4 sm:px-6 flex items-center justify-center lg:px-8">
				<div className="mt-8 md:mt-0 md:order-1">
					<p className="text-center text-base text-gray-400 font-medium">Florian Sautejeau & William Donval</p>
					<p className="text-center text-base text-gray-400">Projet Twitter - 2021</p>
				</div>
			</div>
		</footer>
	);
};

export default Footer;
