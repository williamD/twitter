import React, { useState, useEffect, Fragment } from "react";
import { Listbox, Transition } from "@headlessui/react";

/*const data = [
	{ id: 1, name: "Durward Reynolds", unavailable: false },
	{ id: 2, name: "Kenton Towne", unavailable: false },
	{ id: 3, name: "Therese Wunsch", unavailable: false },
	{ id: 4, name: "Benedict Kessler", unavailable: true },
	{ id: 5, name: "Katelyn Rohan", unavailable: false },
];*/

const Select = ({ label, data, size, setSelected, initialSelected }) => {
	const [selectedItem, setSelectedItem] = useState(initialSelected);
	useEffect(() => {
		setSelected(selectedItem);
	}, [selectedItem]);

	return (
		<div className={`flex items-center justify-center ${size ? size : null}`}>
			<div className="w-full">
				<Listbox as="div" className="space-y-1" value={selectedItem} onChange={setSelectedItem}>
					{({ open }) => (
						<>
							{label && <Listbox.Label className="block text-sm leading-5 font-medium text-gray-700">{label}</Listbox.Label>}
							<div className="relative">
								<span className="inline-block w-full rounded-md shadow-sm">
									<Listbox.Button className="cursor-default relative w-full rounded-md border border-gray-300 bg-white pl-3 pr-10 py-2 text-left focus:outline-none focus:shadow-outline-light-blue focus:border-light-blue-300 transition ease-in-out duration-150 sm:text-sm sm:leading-5">
										<span className="block truncate">{selectedItem.name}</span>
										<span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
											<svg className="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="none" stroke="currentColor">
												<path d="M7 7l3-3 3 3m0 6l-3 3-3-3" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
											</svg>
										</span>
									</Listbox.Button>
								</span>
								{/* Use the Transition + open render prop argument to add transitions. */}
								<Transition
									show={open}
									enter="transition duration-100 ease-out"
									enterFrom="transform scale-95 opacity-0"
									enterTo="transform scale-100 opacity-100"
									leave="transition duration-75 ease-out"
									leaveFrom="transform scale-100 opacity-100"
									leaveTo="transform scale-95 opacity-0"
									className="absolute mt-1 w-full rounded-md bg-white shadow-lg z-20"
								>
									<Listbox.Options
										static
										className="max-h-60 rounded-md py-1 text-base leading-6 shadow-xs overflow-auto focus:outline-none sm:text-sm sm:leading-5"
									>
										{data.map((item) => (
											<Listbox.Option as={Fragment} key={item.id} value={item} disabled={item.unavailable}>
												{({ selected, active }) => (
													<div
														className={`${
															active ? "text-white bg-light-blue-600" : "text-gray-900"
														} cursor-default select-none relative py-2 pl-8 pr-4`}
													>
														<span className={`${selected ? "font-semibold" : "font-normal"} block truncate`}>{item.name}</span>
														{selected && (
															<span
																className={`${
																	active ? "text-white" : "text-light-blue-600"
																} absolute inset-y-0 left-0 flex items-center pl-1.5`}
															>
																<svg
																	className="h-5 w-5"
																	xmlns="http://www.w3.org/2000/svg"
																	viewBox="0 0 20 20"
																	fill="currentColor"
																>
																	<path
																		fillRule="evenodd"
																		d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
																		clipRule="evenodd"
																	/>
																</svg>
															</span>
														)}
													</div>
												)}
											</Listbox.Option>
										))}
									</Listbox.Options>
								</Transition>
							</div>
						</>
					)}
				</Listbox>
			</div>
		</div>
	);
};

export default Select;
