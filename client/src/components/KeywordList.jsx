import React, { useContext } from "react";
import Keyword from "./Keyword";
import { KeyWordsContext } from "@/Home";

const KeywordList = () => {
	const { keyWords } = useContext(KeyWordsContext);
	return (
		<div>
			{keyWords && keyWords.length > 0 ? (
				<>
					<div className="bg-white border-b border-gray-200 flex" aria-label="Breadcrumb">
						<div className="max-w-screen-xl w-full mx-auto px-4 py-2 flex sm:px-6 lg:px-8 justify-center flex-wrap">
							{keyWords.map((el) => {
								return <Keyword key={el} name={el} />;
							})}
						</div>
					</div>
				</>
			) : null}
		</div>
	);
};

export default KeywordList;
