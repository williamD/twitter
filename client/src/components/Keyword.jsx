import React, { useContext } from "react";
import { KeyWordsContext } from "../Home";

const Keyword = ({ name }) => {
	const { removeKeyWord } = useContext(KeyWordsContext);
	return (
		<span className="inline-flex mx-2 my-1 rounded-full items-center py-0.5 pl-2.5 pr-1 text-sm font-medium bg-light-blue-100 text-light-blue-700 space-x-1">
			<span>{name}</span>
			<button
				onClick={() => removeKeyWord(name)}
				type="button"
				className="flex-shrink-0 ml-0.5 h-4 w-4 rounded-full inline-flex items-center justify-center text-light-blue-400 hover:bg-light-blue-200 hover:text-light-blue-500 focus:outline-none focus:bg-light-blue-500 focus:text-white"
			>
				<span className="sr-only">Remove large option</span>
				<svg className="h-2 w-2" stroke="currentColor" fill="none" viewBox="0 0 8 8">
					<path strokeLinecap="round" strokeWidth="1.5" d="M1 1l6 6m0-6L1 7" />
				</svg>
			</button>
		</span>
	);
};

export default Keyword;
