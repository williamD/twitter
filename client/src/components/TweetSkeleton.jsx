import React from "react";

const TweetSkeleton = () => {
	return (
		<div className="cursor-pointer p-3 relative rounded-lg border border-gray-200 bg-white shadow-sm flex items-center flex-col md:flex-row space-x-3 hover:border-gray-300 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
			<div className="sm:self-center md:self-start flex-shrink-0">
				<div className="inline-block h-12 w-12 bg-gray-200 rounded-md"></div>
			</div>
			<div className="flex-1 min-w-0">
				<div className="flex flex-col focus:outline-none">
					<div>
						<span className="absolute inset-0" aria-hidden="true"></span>
						<div className="flex flex-wrap sm:justify-center md:justify-start mb-1.5">
							<p className="h-4 w-16 bg-gray-200 mr-2 rounded-md"></p>
							<p className="h-4 w-20 bg-gray-200 rounded-md"></p>
						</div>
						<div className="space-y-1.5">
							<div className="flex w-full space-x-2">
								<p className="bg-gray-100 h-4 w-4/12 rounded-md"></p>
								<p className="bg-gray-100 h-4 w-5/12 rounded-md"></p>
								<p className="bg-gray-100 h-4 w-3/12 rounded-md"></p>
							</div>
							<div className="flex w-full space-x-2">
								<p className="bg-gray-100 h-4 w-1/2 rounded-md"></p>
								<p className="bg-gray-100 h-4 w-1/2 rounded-md"></p>
							</div>
							<p className="bg-gray-100 h-4 w-1/3 rounded-md"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default TweetSkeleton;
