import axios from "axios";

const instance = axios.create({
	baseURL: "http://william.lpweb-lannion.fr:8000/api",
});

export default instance;
