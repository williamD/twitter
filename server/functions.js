const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

require("./connection.js");

let tweetSchema;
const instanciateSchema = () => {
	tweetSchema = new mongoose.Schema({}, { strict: false });
	tweetSchema.plugin(mongoosePaginate);
};

instanciateSchema();

const sleep = (ms) => {
	return new Promise((resolve) => setTimeout(resolve, ms));
};

const search = async (values, page, time, limit, sort) => {
	const options = {
		page: page ? +page : 1,
		limit: limit ? +limit : 15,
		sort: sort ? sort : { "data.created_at": "desc" },
	};
	const Tweet = mongoose.model("Tweet", tweetSchema);
	const orQuery = values && values.length > 0 ? values.map((value) => ({ "data.text": new RegExp(value, "i") })) : null;
	const result = orQuery ? await Tweet.paginate({ $or: orQuery }, options) : await Tweet.paginate({}, options);
	await sleep(time * 1000);
	return result;
};

const getAll = async (page, time, limit, sort) => {
	const options = {
		page: page ? +page : 1,
		limit: limit ? +limit : 15,
		sort: sort ? sort : { "data.created_at": "desc" },
	};
	const Tweet = mongoose.model("Tweet", tweetSchema);
	const result = await Tweet.paginate({}, options);
	await sleep(time * 1000);
	return result;
};

module.exports.search = search;
module.exports.getAll = getAll;
