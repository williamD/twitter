const mongoose = require("mongoose");
const needle = require("needle");
require("dotenv").config();

mongoose.connect("mongodb://william.lpweb-lannion.fr:8900/twitter", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	auth: {
		authSource: "admin",
	},
	user: "root",
	pass: "example",
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function () {
	const tweetSchema = new mongoose.Schema({}, { strict: false });
	const Tweet = mongoose.model("Tweet", tweetSchema);
	(async function () {
		let currentRules;

		try {
			// Gets the complete list of rules currently applied to the stream
			currentRules = await getAllRules();

			// Delete all rules
			await deleteAllRules(currentRules);

			// Add rules to the stream
			await setRules();
		} catch (error) {
			console.error(error);
			process.exit(1);
		}

		// Listen to the stream.
		streamConnect(0, Tweet);
	})();
});

const token = process.env.TWITTER_TOKEN;

const rulesURL = "https://api.twitter.com/2/tweets/search/stream/rules";
const streamURL =
	"https://api.twitter.com/2/tweets/search/stream?tweet.fields=id,text,attachments,author_id,created_at,geo,lang,source&user.fields=id,name,username,profile_image_url,created_at,verified&media.fields=media_key,type,duration_ms,height,width,preview_image_url,url&expansions=author_id,attachments.media_keys,geo.place_id";

const rules = [
	{
		value: `(laravel OR symfony OR node js OR react js OR vue js OR tailwindcss OR angular js OR mongodb OR mysql OR sqlserver OR gatsby OR graphql OR next js OR nuxt js OR ruby on rails OR ionic OR jquery OR bootstrap) -is:retweet`,
		tag: "web technologies",
	},
	{
		value: `(drupal OR magento OR django OR elasticsearch OR inertia js OR postgresql) -is:retweet`,
		tag: "web technologies 2",
	},
];

async function getAllRules() {
	const response = await needle("get", rulesURL, {
		headers: {
			authorization: `Bearer ${token}`,
		},
	});

	if (response.statusCode !== 200) {
		console.log("Error:", response.statusMessage, response.statusCode);
		throw new Error(response.body);
	}

	return response.body;
}

async function deleteAllRules(rules) {
	if (!Array.isArray(rules.data)) {
		return null;
	}

	const ids = rules.data.map((rule) => rule.id);

	const data = {
		delete: {
			ids: ids,
		},
	};

	const response = await needle("post", rulesURL, data, {
		headers: {
			"content-type": "application/json",
			authorization: `Bearer ${token}`,
		},
	});

	if (response.statusCode !== 200) {
		throw new Error(response.body);
	}

	return response.body;
}

async function setRules() {
	const data = {
		add: rules,
	};

	const response = await needle("post", rulesURL, data, {
		headers: {
			"content-type": "application/json",
			authorization: `Bearer ${token}`,
		},
	});

	if (response.statusCode !== 201) {
		throw new Error(response.body);
	}

	return response.body;
}

function streamConnect(retryAttempt, model) {
	const stream = needle.get(streamURL, {
		headers: {
			"User-Agent": "v2FilterStreamJS",
			Authorization: `Bearer ${token}`,
		},
		timeout: 20000,
	});

	stream
		.on("data", (data) => {
			try {
				const json = JSON.parse(data);
				console.log(json);
				const tweetInstance = new model(json);
				tweetInstance.save();
				// A successful connection resets retry count.
				retryAttempt = 0;
			} catch (e) {
				if (data.detail === "This stream is currently at the maximum allowed connection limit.") {
					console.log(data.detail);
					process.exit(1);
				} else {
					// Keep alive signal received. Do nothing.
				}
			}
		})
		.on("err", (error) => {
			if (error.code !== "ECONNRESET") {
				console.log(error.code);
				process.exit(1);
			} else {
				// This reconnection logic will attempt to reconnect when a disconnection is detected.
				// To avoid rate limits, this logic implements exponential backoff, so the wait time
				// will increase if the client cannot reconnect to the stream.
				setTimeout(() => {
					console.warn("A connection error occurred. Reconnecting...");
					streamConnect(++retryAttempt);
				}, 2 ** retryAttempt);
			}
		});

	return stream;
}
