const express = require("express");
const cors = require("cors");
require("dotenv").config();

const app = express();
app.use(cors());
app.use(express.json());

const port = process.env.API_PORT || 8080;

const routes = require("./routes.js");
routes(app);

app.listen(port, () => console.log(`Server listening on port ${port}`));
