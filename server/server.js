const express = require("express");
const Twitter = require("twitter");
const cors = require("cors");

require("dotenv").config();
const apiv2 = require("./api.js");

const app = express();
app.use(cors());
const port = 8000;

/* Twitter API connection */

const client = new Twitter({
    consumer_key: process.env.API_KEY,
    consumer_secret: process.env.API_SECRET_KEY,
    bearer_token: process.env.BEARER_TOKEN,
});

/*client.get("/2/users/:id", { id: 1200 }, function (error, tweet, response) {
    console.log(tweet); // Tweet body.
});

/* express API */

app.get("/", (req, res) => {
    res.send("Get tweet");
});

app.get("/search", async (req, res) => {
    const searchQuery = req.query.query;
    try {
        const response = await apiv2.get(
            `/tweets/search/recent?query=${searchQuery}`
        );
        res.send(response.data);
    } catch (error) {
        console.error(error);
    }
});

app.listen(port, () => {
    console.log(port);
});
