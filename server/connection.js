/*const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

mongoose.connect("mongodb://william.lpweb-lannion.fr:8900/twitter", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	auth: {
		authSource: "admin",
	},
	user: "root",
	pass: "example",
});

const db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error:"));
db.once("open", async function () {
	const tweetSchema = new mongoose.Schema({}, { strict: false });
	tweetSchema.plugin(mongoosePaginate);
	const Tweet = mongoose.model("Tweet", tweetSchema);

	//const docs = await Tweet.find({ "data.text": /laravel/i });

	Tweet.paginate({ "data.text": /wordpress/i }).then(function (result) {
		console.log(result);
	});
});*/
// tmp

const mongoose = require("mongoose");

mongoose.Promise = Promise;

mongoose.connection.on("connected", () => {
	console.log("Connection Established");
});

mongoose.connection.on("reconnected", () => {
	console.log("Connection Reestablished");
});

mongoose.connection.on("disconnected", () => {
	console.log("Connection Disconnected");
});

mongoose.connection.on("close", () => {
	console.log("Connection Closed");
});

mongoose.connection.on("error", (error) => {
	console.log("ERROR: " + error);
});

const run = async () => {
	await mongoose.connect("mongodb://william.lpweb-lannion.fr:8900/twitter", {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		keepAlive: true,
		auth: {
			authSource: "admin",
		},
		user: "root",
		pass: "example",
	});
};

run().catch((error) => console.error(error));
