const { search, getAll } = require("./functions.js");

module.exports = (app) => {
	app.post("/api/search", async (req, res) => {
		const result = await search(req.body.query, req.body.page, req.body.sub?.time, req.body.limit, req.body.sort);
		res.send(result);
	});
	app.post("/api/tweets", async (req, res) => {
		const result = await getAll(req.body.page, req.body.sub?.time, req.body.limit, req.body.sort);
		res.send(result);
	});
};
